# 3) Crie uma função, que dado um hash como parâmetro, e retorne um array com todos os valores que estão no hash elevados ao quadrado.
# Ex: Entrada: {:chave1 => 5, :chave2 => 30, :chave3 => 20}
#     Saída: [25, 900, 400]

def hashsqrt(x)
    arrResults = []
    x.each do |key,num|
        arrResults.append(num**2)
    end
    print arrResults
end

x = {:key1 => 5, :key2 => 30, :key3 => 20}
puts hashsqrt(x)