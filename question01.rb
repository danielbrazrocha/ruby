# 1) Crie uma função que dado um array de arrays, imprima na tela a soma e a multiplicação de todos os valores. 
# Ex: Entrada: [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
#     Impressão na tela: Soma: 40
#     Impressão na tela: Multiplicação: 100800

def summultarray(x)    
    s = 0 
    m = 1
    for i in x
        for num in i
            s += num
            m *= num
        end
    end
    puts "Soma: #{s}"
    puts "Multiplicação: #{m}"
    
end

x = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
puts summultarray(x)