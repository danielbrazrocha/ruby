# 2) Crie uma função que dado dois valores, retorne a divisão deles (a divisão deve retornar um float).
# Ex: Entrada: 5, 2
#     Saída: 2.5

def division(x, y)
    x = x.to_f
    y = y.to_f
    result = x/y
end

x = 5
y = 2
puts division(5, 2)